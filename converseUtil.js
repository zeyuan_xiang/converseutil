//服务器配置
var SERVER = 'http://localhost:3000';
var BOSH_SERVICE = 'http://localhost:5280/http-bind/';

var conn = new Strophe.Connection(BOSH_SERVICE);
var jid = null;
var socket = io.connect(SERVER);

/*
	autologin
*/
function chat_login(chat_username, chat_password){
    var post_data = {
        name: chat_username,
        pwd: chat_password
    };
	$.ajax({
        type: 'POST',
        url: SERVER+"/prebind", //从服务器获得授权
        data: post_data,
        success: function(data){
            console.log(data);
            jid = data.jid;
            
            require(['converse'], function (converse) {
                converse.initialize({
                    auto_list_rooms: true,
                    auto_subscribe: false,
                    bosh_service_url: BOSH_SERVICE,
                    webservice_url: SERVER,
                    hide_muc_server: false,
                    i18n: locales.en,
                    prebind: true,
                    keepalive: true,
                    jid: data.jid,
                    sid: data.sid,
                    rid: data.rid,
                    debug: false,
                    expose_rid_and_sid: true,
                    show_controlbox_by_default: true,
                    roster_groups: true
                });
            });
            // setTimeout("autojoin()", 5000);
            //wait to join
            socket.on(jid.split('/')[0], function (room) {
                console.log(room);
                // join(room, jid.split('/')[0]);
            });
        }
    });

}


/*
    进入聊天室
    必须实现incomingMessageHandler,groupPresenceHandler,onRoster方法
*/	
function join(muc_room_name) {
    function incomingMessageHandler(msg){
        console.log(msg);
    };

    function groupPresenceHandler(presence){
        console.log(presence);
    };

    function onRoster(roster){
        console.log(roster);
    };

    conn.attach(jid, converse.getSID(), converse.getRID()+1, function(status) {
        try {
            conn.muc.join(
                muc_room_name,
                jid.split('/')[0],
                incomingMessageHandler,
                groupPresenceHandler,
                onRoster
            );
        } catch (e) {
            console.error(e);
        }
	});
};



/*
	function: create muc
	params: doctor_chat_user_name  
			doctor_display_name ==> muc_room_name
			chat_user_list ==> invite_list
            if doctor_display_name != doctor_chat_user_name
                create muc
            else
                join muc
*/
function chat_create_group(doctor_chat_user_name, doctor_display_name, chat_user_list){
    // join self
    join(doctor_display_name);
    //join chat_user_list
    socket.emit('invite', { room: doctor_display_name, receivers: chat_user_list });
    
    if(doctor_display_name != doctor_chat_user_name){
        chat_user_list.push(jid);
        var post_data = {
            names: chat_user_list,
            room: doctor_display_name
        };
        console.log(post_data);
        $.ajax({
            type: 'POST',
            url: SERVER+"/joinmuc",
            data: post_data,
            success: function(data){
                // console.log();
            }
        });
    }
}

